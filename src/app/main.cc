#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>
#include <set>

#include <cstring>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "helper.hh"
#include "searcher.hh"
#include "result.hh"

void print_results(std::vector<Result>& results)
{
    std::sort(results.begin(), results.end());

    auto words = std::set<std::string>();

    if (results.size() > 0)
    {
        std::cout << results[0];
        words.insert(results[0].word);
        delete[] results[0].word;

        for (auto it = ++results.begin(); it != results.end(); ++it)
        {
            auto res = *it;
            if (words.find(res.word) == words.end())
            {
                std::cout << "," << res;
                words.insert(res.word);
            }
            delete[] res.word;
        }
    }
}

void search(void *dict, int max_dist, const std::string& word)
{
    char *w = new char[word.length() + 1]();
    strcpy(w, word.c_str());

    auto searcher = Searcher((char*)dict, max_dist);
    searcher.distance(w);

    print_results(searcher.results);

    delete[] w;
}

int main(int argc, char **argv)
{
    std::cerr << "App\n";
    if (argc != 2)
    {
        std::cerr << "Usage: cat test.txt | " << argv[0]
                  << " /path/to/dict.bin\n";
        return 1;
    }

    int fd = open(argv[1], O_RDONLY);
    struct stat st;
    fstat(fd, &st);
    void *dict = mmap(nullptr, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
    close(fd);

    std::string line;
    while (std::getline(std::cin, line))
    {
        std::stringstream ss(line);
        std::string approx, distance, word;
        std::getline(ss, approx, ' ');
        std::getline(ss, distance, ' ');
        std::getline(ss, word, ' ');
        if (approx == "approx")
        {
            int dist = std::stoi(distance);
            std::cout << "[";
            search(dict, dist, word);
            std::cout << "]" << std::endl;
        }
    }
    return 0;
}
