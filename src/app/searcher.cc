#include "searcher.hh"

Searcher::Searcher(const char *dict, int max_dist)
 : dict(dict), max_dist(max_dist)
{
    this->hist = new char[1024]();
    this->results = std::vector<Result>();
}

Searcher::~Searcher()
{
    delete[] hist;
}

void Searcher::distance(char *word,
                        size_t i_word, size_t i_node,
                        int dist, size_t hist_index,
                        bool ignore_current_char)
{
    if (dist > max_dist)
        return;

    char current_char = get_char(dict, i_node);
    unsigned freq = get_freq(dict, i_node);
    size_t nb_children = get_nb_children(dict, i_node);

    // Save current character
    if (current_char != '\0' && !ignore_current_char)
    {
        hist[hist_index] = current_char;
        ++hist_index;
    }

    // Result found
    if (word[i_word] == '\0' && freq > 0)
    {
        char *res_word = new char[hist_index + 1]();
        results.emplace_back(strncpy(res_word, hist, hist_index), freq, dist);
    }

    // Deletion
    if (word[i_word] != '\0')
        distance(word, i_word + 1, i_node, dist + 1, hist_index, true);

    for (size_t i_child = 0; i_child < nb_children; ++i_child)
    {
        size_t child_offset = get_child_offset(dict, i_node, i_child);
        char child_char = get_char(dict, child_offset);

        int mdist = !(strlen(word + i_word) > 0 && child_char == word[i_word]);

        // Substitution
        if (word[i_word] != '\0')
            distance(word, i_word + 1, child_offset, dist + mdist, hist_index);

        // Insertion
        distance(word, i_word, child_offset, dist + 1, hist_index);

        // Transposition
        if (i_word > 0
            && word[i_word - 1] == child_char
            && word[i_word] == current_char)
        {
            std::swap(word[i_word - 1], word[i_word]);
            distance(word, i_word + 1, child_offset, dist, hist_index);
            std::swap(word[i_word - 1], word[i_word]);
        }
    }
}
