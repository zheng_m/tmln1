#pragma once

#include <cstddef>

char get_nb_children(const char *dict, size_t i);

char get_char(const char *dict, size_t i);

unsigned get_freq(const char *dict, size_t i);

size_t get_child_offset(const char *dict, size_t i_node, size_t i_child);
