#pragma once

#include <cstring>
#include <iostream>

struct Result
{
    char *word;
    unsigned freq;
    int dist;

    Result(char *word, unsigned freq, int dist) :
        word(word), freq(freq), dist(dist)
    {}

    bool operator< (const struct Result& other) const
    {
        if (dist < other.dist)
            return true;

        if (dist == other.dist)
        {
            if (freq > other.freq)
                return true;
            if (freq == other.freq)
                return strcmp(word, other.word) < 0;
        }
        return false;
    }

    friend std::ostream& operator<< (std::ostream& os, const struct Result& other)
    {
        os << "{\"word\":\"" << other.word
           << "\",\"freq\":" << other.freq
           << ",\"distance\":" << other.dist
           << "}";

        return os;
    }
};
