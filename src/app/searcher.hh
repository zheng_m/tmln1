#pragma once

#include <cstddef>
#include <vector>

#include "helper.hh"
#include "result.hh"

class Searcher
{
public:
    Searcher(const char *dict, int max_dist);
    ~Searcher();
    Searcher(const Searcher& other) = delete;
    Searcher& operator=(const Searcher& other) = delete;

    void distance(char *word,
                  size_t i_word = 0,
                  size_t i_node = 0,
                  int dist = 0,
                  size_t hist_index = 0,
                  bool ignore_current_char = false);

    std::vector<Result> results;

private:
    const char *dict;
    int max_dist;
    char *hist;
};
