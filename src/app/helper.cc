#include "helper.hh"

char get_nb_children(const char *dict, size_t i)
{
    return dict[i];
}

char get_char(const char *dict, size_t i)
{
    return dict[i + 1];
}

unsigned get_freq(const char *dict, size_t i)
{
    dict += i + 2;
    unsigned *p = (unsigned *)dict;
    return *p;
}

size_t get_child_offset(const char *dict, size_t i_node, size_t i_child)
{
    dict += i_node + 2 + sizeof (unsigned);
    unsigned *children_offsets = (unsigned *)dict;
    return children_offsets[i_child];
}
