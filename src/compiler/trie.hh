#pragma once

#include <fstream>
#include <iostream>
#include <map>
#include <memory>

struct Node
{
    ~Node();

    std::map<char, Node*> children;
    unsigned freq = 0;
};

class Trie
{
public:
    Trie();

    void insert(const std::string& word, unsigned int freq);
    void print(std::string s = std::string("")) const;
    void serialize(const std::string& out_path) const;

private:
    void print_rec(std::string s, Node* n) const;
    unsigned serialize_rec(Node* n, char c, char *buf, size_t i) const;
    void update_size();
    std::unique_ptr<Node> root;
    size_t size = 0;
};
