#include "trie.hh"

#include <assert.h>

Node::~Node()
{
    for (const auto& pair : children)
    {
        Node* n = pair.second;
        if (n != nullptr)
            delete n;
    }
}

Trie::Trie()
{
    root = std::make_unique<Node>(Node());
    update_size();
    size -= sizeof (unsigned); // root has no parent
}

void Trie::update_size()
{
    /* Block structure:
    ** - nb_children (char)
    ** - character (char)
    ** - frequency (unsigned)
    ** - offsets (unsigned[])
    */
    size += 2 * sizeof (char) + 2 * sizeof (unsigned);
}

void Trie::insert(const std::string& word, unsigned freq)
{
    Node* cur = root.get();
    if (!cur)
        return;

    for (const char c : word)
    {
        Node* child = cur->children[c];
        if (child == nullptr)
        {
            child = new Node();
            cur->children[c] = child;
            update_size();
        }
        cur = child;
    }
    cur->freq = freq;
}

void Trie::print(std::string prefix) const
{
    this->print_rec(prefix, root.get());
}

void Trie::print_rec(std::string prefix, Node* cur) const
{
    // for debug
    if (cur->freq > 2147000000)
    {
        std::cerr << prefix << " (" << cur->freq << ")\n";
    }
    for (const auto& pair : cur->children)
    {
        Node* child = pair.second;
        if (child != nullptr)
        {
            this->print_rec(prefix + std::string(1, pair.first), child);
        }
    }
}

void Trie::serialize(const std::string& out_path) const
{
    std::ofstream output(out_path, std::ofstream::binary);
    char *buf = new char[size];

    size_t index = 0;
    unsigned buf_size = serialize_rec(root.get(), 0, buf, index);

    assert(size == buf_size);

    output.write(buf, size);

    delete[] buf;
    output.close();

    std::cerr << "Trie size (disk) = " << size << std::endl;
}


unsigned get_block_size(Node* n)
{
    unsigned block_size = 0;

    block_size += sizeof (char); // nb_children

    block_size += sizeof (char); // character

    block_size += sizeof (unsigned); // frequency

    char nb_children = n->children.size();
    block_size += nb_children * sizeof (unsigned); // offsets

    return block_size;
}

unsigned Trie::serialize_rec(Node* n, char c, char* buf, size_t i) const
{
    char *p;
    unsigned *p_uint;

    char nb_children = n->children.size();
    buf[i] = nb_children;
    //std::cerr << "buf[" << i << "] = " << (int)nb_children << std::endl;
    i += sizeof (nb_children);

    buf[i] = c;
    //std::cerr << "buf[" << i << "] = " << c << std::endl;
    i += sizeof (c);

    unsigned freq = n->freq;
    p = buf + i;
    p_uint = (unsigned *) p;
    *p_uint = freq;
    //std::cerr << "buf[" << i << "] = " << freq << "=" << *p_uint << std::endl;
    i += sizeof (freq);

    //std::cerr << "blk: " << (int)nb_children << " children. char: " << c << " freq:" << freq << "\n";
    unsigned next_block_index = i + sizeof (unsigned) * nb_children;

    for (const auto& pair : n->children)
    {
        c = pair.first;
        n = pair.second;
        p = buf + i;
        p_uint = (unsigned *) p;
        *p_uint = next_block_index;
        i += sizeof (next_block_index);
        //std::cerr << "next: " << next_block_index << "\n";
        next_block_index = serialize_rec(n, c, buf, next_block_index);
    }

    return next_block_index;
}
