#include <iostream>
#include <fstream>

#include "trie.hh"

Trie parse_input(const std::string& path)
{
    Trie trie;

    std::ifstream input(path);
    std::string word;
    unsigned freq;
    while (!input.eof())
    {
        input >> word;
        input >> freq;
        trie.insert(word, freq);
    }
    input.close();

    return trie;
}

int main(int argc, char **argv)
{
    std::cerr << "Compiler\n";
    if (argc != 3)
    {
        std::cerr << "Usage: " << argv[0]
                  << " /path/to/words.txt /path/to/dict.bin\n";
        return 1;
    }

    std::string words_freq_path = argv[1];
    std::string out_dict_path = argv[2];

    Trie trie = parse_input(words_freq_path);

    trie.serialize(out_dict_path);

    return 0;
}
