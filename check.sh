#!/bin/bash

QUERIES="tests/queries.txt"
APP_REF="../ref/TextMiningApp"
APP_MINE="build/TextMiningApp"
DICT_REF="../ref/dict.bin"
DICT_MINE="build/dict.bin"

if [[ -n "$1" ]]; then
    QUERIES="$1"
fi
if [[ -n "$2" ]]; then
    APP_REF="$2"
fi
if [[ -n "$3" ]]; then
    DICT_REF="$3"
fi
if [[ -n "$4" ]]; then
    APP_MINE="$4"
fi
if [[ -n "$5" ]]; then
    DICT_MINE="$5"
fi

cat "$QUERIES" | "$APP_REF" "$DICT_REF" 2> /dev/null > /tmp/outRef
cat "$QUERIES" | "$APP_MINE" "$DICT_MINE" 2> /dev/null > /tmp/outMine
diff /tmp/outRef /tmp/outMine
